import { writable } from 'svelte/store';
import { supabaseClient } from '$lib/supabaseClient.js';
export const gsales = writable();
export const gOsales = writable();
export const sale_stats = writable();
export const sales_coord = writable();
export const user_zip = writable('');
export const user_coord = writable();

export async function loadGSales() {
	const { data, error } = await supabaseClient.from('sales').select();
	if (error) {
		return console.error(error);
	}
	gsales.set(data);
}

export async function loadOneGSales(id: string) {
	const { data, error } = await supabaseClient.from('sales').select().eq('id', id);
	if (error) {
		return console.error(error);
	}
	gOsales.set(data);

	getSaleInfo(id);
}

// export async function loadZipSales(zip: number) {
// 	const { data, error } = await supabaseClient.from('sales').select().eq('zip', zip);
// 	if (error) {
// 		return console.error(error);
// 	}
// 	gsales.set(data);
// }

export async function addGSales(gsale: any) {
	const { error } = await supabaseClient.from('sales').insert(gsale);

	if (error) {
		return console.error(error);
	}
}

export async function getSaleInfo(gsale: any) {
	const { data, error } = await supabaseClient.from('sale_info').select().eq('sale_id', gsale);

	if (error) {
		return console.error(error);
	}

	 sale_stats.set(data);
}

// export async function getSaleCoord() {
// 	const { data, error } = await supabaseClient.from('sales').select('coordinates');

// 	if (error) {
// 		return console.error(error);
// 	}

// 	sales_coord.set(data);
// }

export async function getcoorszip(z: any) {
	const response = await fetch(`https://zip-geolocation.buyed.workers.dev/`, {
		headers: {
			'x-zip': z
		}
	});
	let reg = /(.*),(.*)/;
	let x = await response.text();
	let y = x.match(reg);
	if (y != null) {
		let coord:number[] = [Number(y[2]), Number(y[1])];
		user_coord.set(coord);
	} else {
		console.log('Regex failed');
	}
}
